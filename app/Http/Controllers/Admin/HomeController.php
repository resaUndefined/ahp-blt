<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\BobotKriteria;
use App\Models\Alternatif;
use App\Models\NilaiAlternatif;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $alternatif = Alternatif::whereHas('nilai_alternatif')->get();
        $kriteria = Kriteria::all(['id','nama']);
        foreach ($alternatif as $key => $value) {
            $data = [];
            foreach ($kriteria as $key2 => $value2) {
                $tmp = NilaiAlternatif::join('alternatif','alternatif.id','=','nilai_alternatif.id_alternatif')
                                    ->join('sub_kriteria','sub_kriteria.id','=','nilai_alternatif.id_sub_kriteria')
                                    ->join('kriteria','kriteria.id','=','sub_kriteria.id_kriteria')
                                    ->where([
                                        'nilai_alternatif.id_alternatif' => $value->id,
                                        'sub_kriteria.id_kriteria' => $value2->id
                                    ])->select([
                                        'sub_kriteria.nama'
                                    ])->first();
                $data[$value2->id] = $tmp->nama;
            }
            $value->data = $data;
        }
        
        $this->data['alternatif'] = $alternatif;
        $this->data['kriteria'] = $kriteria;

        return view('home', $this->data);
    }
}
