<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Kriteria;

class KriteriaController extends Controller
{
    public function index()
    {
        $data = Kriteria::all();
        return view('kriteria.kriteria')->with('data', $data);
    }

    public function create()
    {
        return view('kriteria.tambah_kriteria');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required'
         ]);

        $data = new Kriteria;
        $data->nama = request('nama');
        $data->save();
        return redirect()->route('kriteria.index')->with('success',
        'Berhasil menambah data');
    }

    public function edit($id)
    {
        $data = Kriteria::where('id', $id)->first();
        if($data){
            return view('kriteria.edit_kriteria')->with('data', $data);
        }
        return redirect()->route('kriteria.index');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama' => 'required'
         ]);

        $data = Kriteria::where('id', $id)->first();
        if($data){
            $data->nama = request('nama');
            $data->save();
        }
        return redirect()->route('kriteria.index')->with('success',
        'Berhasil mengubah data');
    }

    public function destroy($id)
    {
        $data = Kriteria::where('id', $id)->first();
        if($data){
            $data->delete();
        }
        return redirect()->route('kriteria.index')->with('danger',
        'Berhasil menghapus data');
    }
}
