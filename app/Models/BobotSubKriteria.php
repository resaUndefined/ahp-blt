<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class BobotSubKriteria extends Model
{
    protected $fillable = [
        'id_sub_kriteria_1', 'nilai', 'id_sub_kriteria_2'
    ];
    protected $table = "bobot_sub_kriteria";
    public $timestamps = false;

    public function subKriteria1()
    {
        return $this->belongsTo('App\Models\SubKriteria', 'id_sub_kriteria_1', 'id');
    }
    public function subKriteria2()
    {
        return $this->belongsTo('App\Models\SubKriteria', 'id_sub_kriteria_2', 'id');
    }
}
