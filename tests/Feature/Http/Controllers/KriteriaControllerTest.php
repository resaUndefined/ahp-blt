<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Kriteria;

class KriteriaControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTambahKriteria()
    {
        Kriteria::create([
            'nama' => 'Harga'
        ]);
        $getKriteria = Kriteria::where('nama','like','%Harga')->first();
        $status = false;
        if (!is_null($getKriteria)) {
            $status = true;
        }
        $this->assertTrue($status);
    }

    public function testEditKriteria()
    {
        Kriteria::create([
            'nama' => 'harga'
        ]);
        Kriteria::create([
            'nama' => 'layar'
        ]);
        Kriteria::create([
            'nama' => 'batere'
        ]);
        $getKriteriaTerakhir = Kriteria::latest('id')->first();
        $getKriteriaTerakhir->nama = 'baterai';
        $getKriteriaTerakhir->save();
        $status = null;
        $checkKriteriaTerakhir = Kriteria::latest('id')->first();
        if ($checkKriteriaTerakhir->nama == 'batere') {
            $status = false;
        }
        if ($checkKriteriaTerakhir->nama == 'baterai') {
            $status = true;
        }

        $this->assertTrue($status);
    }

    public function testHapusKriteria()
    {
        Kriteria::create([
            'nama' => 'layar'
        ]);

        $rootKriteria = Kriteria::where('nama','layar');
        $status = false;
        if (!is_null($rootKriteria->first())) {
            $status = true;
        }
        $this->assertTrue($status);
        $rootKriteria->delete();
        $checkLagi = Kriteria::where('nama','layar')->first();
        if (is_null($checkLagi)) {
            $status = false;
        }

        $this->assertFalse($status);
        
    }
}
