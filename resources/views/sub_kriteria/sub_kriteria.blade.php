@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">SUB KRITERIA <span class="badge badge-secondary" data-toggle="tooltip" data-placement="top" title="Ubah">{{$data->nama}}</span></h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/kriteria">Sub Kriteria</a></li>
                <li class="breadcrumb-item active">{{$data->nama}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="40">NO</th>
                        <th>NAMA</th>
                        <th>PARAMETER</th>
                        <th width="80">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data->sub_kriteria as $krit)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{$krit->nama}}</td>
                        <td>{{$krit->parameter}}</td>
                        <td>
                            <a href="/sub_kriteria/{{$krit->id}}/edit">
                            <span class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-pencil"></i></span>
                            </a>
                            <form id='delete-kriteria{{$krit->id}}' action="/sub_kriteria/{{$krit->id}}" method="post" style="display: inline;">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@include ('includes.script')
<script type="text/javascript">
$(document).ready(function(){
    $("#data-admin_length").append('<a  href="{{ route('sub_kriteria.create', ['id_kriteria' => $data->id]) }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
});
</script>
@endsection
